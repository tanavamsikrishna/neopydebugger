"""
NeoVim remote plugin to interface with PDB
Essentially an adapter between NeoVim & PDB
"""
import pynvim
import pexpect


@pynvim.plugin
class PDBDebugger:
    """ Class to handle PDB """

    PDB_PROMPT = "(Pdb) "
    BREAK_CLEAR_CONFIRMATION = "Clear all breaks? "
    ERROR_FORMAT = r">\ %f(%l)%m"

    def __init__(self, nvim):
        self.nvim = nvim
        self.process = None
        self.prompt = ""

    @pynvim.command("PdbStart", nargs="*")
    def start(self, args):
        """ To start a debugging session """
        self.prompt = PDBDebugger.PDB_PROMPT
        self.process = pexpect.spawn("pipenv run python -m pdb " + " ".join(args))
        self.process.expect_exact(PDBDebugger.PDB_PROMPT)
        self.nvim.command("set errorformat={}".format(self.ERROR_FORMAT))
        self.nvim.command('silent cexpr []')
        self.prepare_ui(self.process_pdb_output())
        self.nvim.command("copen")
        self.repl()

    @pynvim.command("PdbCommand", nargs="*")
    def command(self, args):
        """ Run commands as understood by this debugger"""
        if len(args) == 0:
            self.repl()
        else:
            self.run_pdb_command(args)

    def quit(self):
        self.process.sendline("quit")
        self.process.close(force=True)
        self.process = None
        self.nvim.out_write("Debugger Stopped\n")
        self.nvim.command("cclose | match None")

    def repl(self, open_qf=False):
        while True:
            # Read
            command = self.nvim.command_output(
                "echo input(\'{}\')".format(self.prompt)
            )
            # Evaluate
            if self.prompt == PDBDebugger.PDB_PROMPT:
                if command == "exit" or command == "quit" or command == "q":
                    self.quit()
                    break
                elif command == "pause":
                    self.nvim.command("match None")
                    self.nvim.out_write("Debugger Paused\n")
                    break
            prompt = self.run_pdb_command(command)
            new_current_line = self.process_pdb_output()
            # "Print" part
            self.prepare_ui(new_current_line)
            # Loop (prepare to read)
            if prompt == 0:
                self.prompt = PDBDebugger.PDB_PROMPT
            else:
                self.prompt = PDBDebugger.BREAK_CLEAR_CONFIRMATION

    def run_pdb_command(self, args):
        """ To step through one statement """
        command = "".join(args).strip()
        self.process.sendline(command)
        return self.process.expect_exact([
            self.PDB_PROMPT,
            self.BREAK_CLEAR_CONFIRMATION
        ])

    def process_pdb_output(self):
        """
        To parse the output of PDB into a format useable by quicfix window
        returns text in current line
        """
        pdb_output = self.process.before.decode("utf-8")
        pdb_output_lines = pdb_output.strip().split("\r\n")
        pdb_output_lines = list(filter(lambda line: len(line) > 0, pdb_output_lines))
        if not pdb_output_lines:
            return None
        if pdb_output_lines[-1].startswith("-> "):
            new_current_line = pdb_output_lines[-1][3:]
        else:
            new_current_line = None
        pdb_output_lines.append("")
        quickfix_input = [i.replace(r"\*", r"\\\*") for i in pdb_output_lines]
        quickfix_input = ['"{}"'.format(i) for i in quickfix_input]
        quickfix_input = ",".join(quickfix_input)
        quickfix_input = "[{}]".format(quickfix_input)
        nvim_command = "silent caddexpr {}".format(quickfix_input)
        self.nvim.command(nvim_command)
        return new_current_line

    def prepare_ui(self, new_current_line=None):
        """ Handle Ui Changes after completion of previous PDB command """
        self.nvim.command("silent clast")
        if new_current_line is None:
            return
        nvim_command = "silent cprevious | silent normal! zz"
        self.nvim.command(nvim_command)
        new_current_line = new_current_line.replace("/", r"\/")
        self.nvim.command(r"silent match CursorLine /\V{}/".format(new_current_line))
