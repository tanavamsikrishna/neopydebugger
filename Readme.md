# NeoPyDebugger
> A clean & useable integration of PDB, the python debugger, with NeoVim

The plugin aims at being quick to use without any loss of PDB's functionality

## Installation
If you are using vim-plug, add the following to vimrc

`Plug 'https://bitbucket.org/tanavamsikrishna/neopydebugger'`

Then run `nvim +"UpdateRemotePlugins" +"qa"`

## Features (& Aim)
- Easy access to frequently used debugger commands
- No Overhead
- Minimal introduction of terminology which is not PDB's or Python's
- Log of debugging steps & output (and easy navigation through it)
- Of course, auto jump to the line of code being debugged

## Example Usage (Link to a youtube video)
[![NeoPyDebuggerDemo](https://img.youtube.com/vi/qOM2Bn7Lvts/0.jpg)](https://www.youtube.com/watch?v=qOM2Bn7Lvts)

## Configuration
#### Planned
- TODO: setting path to a non-default python executable
- TODO: setting for auto open debug trace buffer

## Meta
Vamsi Talupula
[Bitbucket](https://bitbucket.org/tanavamsikrishna)
[Website](http://vamsitalupula.com)
